import json
import requests
import pprint
import datetime

pp = pprint.PrettyPrinter(width=41, compact=True)



def get_yesterday_data():

    previousDate = datetime.datetime.today() - datetime.timedelta(days=1)
    previousDate = previousDate.date()
    previousDate = str(previousDate).replace('-', '')

    apiURL = 'https://covidtracking.com/api/v1/states/KY/'+str(previousDate)+'.json'

    response = requests.get(apiURL)

    if response.status_code == 200:
        return (json.loads(response.content.decode('utf-8')))

        
    else:
        print ("noooooooo")
        print (response.status_code)
        return False



def get_current_data():
    apiURL = 'https://covidtracking.com/api/v1/states/current.json'

    response = requests.get(apiURL)

    if response.status_code == 200:
        return (json.loads(response.content.decode('utf-8')))

        
    else:
        print ("noooooooo")
        print (response.status_code)
        return false





def get_historical_data():

    previousDate = datetime.datetime.today() - datetime.timedelta(days=8)
    previousDate = previousDate.date()
    previousDate = str(previousDate).replace('-', '')

    apiURL = 'https://covidtracking.com/api/v1/states/KY/'+str(previousDate)+'.json'

    response = requests.get(apiURL)

    if response.status_code == 200:
        return (json.loads(response.content.decode('utf-8')))

        
    else:
        print ("noooooooo")
        print (response.status_code)
        return False



# --------------------------------------- Main ---------------------------------------
# 17 elemment is Kentucky
loadedJSONCurrent = get_current_data()

for l in loadedJSONCurrent:
    if l["state"] == "KY":
        # pp.pprint(l)

        currentPositive = l["positive"]
        currentDeath = l["death"]
        currentTotalTests = l["totalTestResults"]
        currentRecovered = l["recovered"]





print("Yesterday Increase: ")
JSONYesterday = get_yesterday_data();
positiveIncrease = JSONYesterday['positiveIncrease']
testResultsIncrease = JSONYesterday['totalTestResultsIncrease']
deathIncrease = JSONYesterday['deathIncrease']

print("Positive Increase: "+str(positiveIncrease))
print("Death Increase: "+str(deathIncrease))
print("Testing Increase: "+str(testResultsIncrease))


print ("--------------------------------------------")

print ("8 days ago: ")

loadedJSONHistory = get_historical_data()

positiveIncreaseHistory = loadedJSONHistory['positiveIncrease']
testResultsIncreaseHistory = loadedJSONHistory['totalTestResultsIncrease']
deathIncreaseHistory = loadedJSONHistory['deathIncrease']

print("Positive Increase: "+str(positiveIncreaseHistory))
print("Death Increase: "+str(deathIncreaseHistory))
print("Testing Increase: "+str(testResultsIncreaseHistory))



print ("--------------------------------------------")

today = datetime.date.today()
print("Today's date:", today)

print("Positive: "+str(currentPositive))
print("Recovered: "+str(currentRecovered))
print("Death: "+str(currentDeath))
print("Total Tests: "+str(currentTotalTests))



print ("--------------------------------------------")






